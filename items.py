from datape.indicators.models import Indicator
from datape.utils import clean_name
from scrapy_djangoitem import DjangoItem
from slugify import slugify


def get_instance(node_id):
    return Indicator.objects.get(pk=node_id)


class IndicatorItem(DjangoItem):
    django_model = Indicator

    @staticmethod
    def create_simple_indicator(response, parent=None):
        name = response.css("span.ui-treenode-label.ui-corner-all span::text").get()
        row_key = (
            response.css("::attr(data-rowkey)").extract_first()
            if not parent
            else response.css("::attr(data-rowkey)").get()
        )
        data = {
            "name": name.strip().title(),
            "slug": slugify(f"{name} {row_key}"),
            "row_key": row_key,
        }
        return (
            get_instance(parent.pk).add_child(**data)
            if parent
            else Indicator.add_root(**data)
        )

    @staticmethod
    def create_indicator(response, parent=None, default=None):
        image = response[15].xpath('//img[@id="j_idt489"]/@src').get()
        formula = f'{default.get("base_url")}{image}' if image else ""
        data = {
            "name": clean_name(default.get("name")),
            "slug": slugify(f'{default.get("name")} {default.get("row_key")}'),
            "row_key": default.get("row_key"),
            "topic": str(response[3].xpath("text()").get()).title() or "",
            "definition": response[5].xpath("text()").get() or "",
            "unit_measurement": response[7].xpath("text()").get() or "",
            "periodicity": response[9].xpath("text()").get() or "",
            "ambit": response[11].xpath("text()").get() or "",
            "use": response[13].xpath("text()").get() or "",
            "formula": formula,
            "disintegration": response[17].xpath("text()").get() or "",
            "observation": response[19].xpath("text()").get() or "",
            "source": response[21].xpath("text()").get() or "",
        }
        return get_instance(parent.pk).add_child(**data)
