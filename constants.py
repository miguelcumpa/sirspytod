from enum import Enum


class TYPE_REQUEST(Enum):
    INDICATOR = "INDICATOR"
    DETAIL_INDICATOR = "DETAIL_INDICATOR"
    TABLE_PARAMETER = "TABLE_PARAMETER"
    EXCEL_PARAMETER = "EXCEL_PARAMETER"


SIRTOD_BASE_URL = "http://webapp.inei.gob.pe:8080"

SCRAPING_DOMAINS = ["webapp.inei.gob.pe"]

INDICATOR_PARAMETERS = {
    "javax.faces.partial.ajax": "true",
    "javax.faces.source": "formIzquierda:idArbolIndicadores",
    "javax.faces.partial.execute": "formIzquierda:idArbolIndicadores",
    "javax.faces.partial.render": "formIzquierda:idArbolIndicadores",
    "javax.faces.behavior.event": "expand",
    "javax.faces.partial.event": "expand",
    "formIzquierda:idArbolIndicadores_expandNode": "",
    "formIzquierda": "formIzquierda",
    "formIzquierda:j_idt37": "0",
    "formIzquierda:j_idt51": "-1",
    "formIzquierda:j_idt78": "1",
    "formIzquierda:idAnioInicio": "1940",
    "formIzquierda:idAnioFin": "4279",
    "formIzquierda:idArbolIndicadores_selection": "",
    "javax.faces.ViewState": "",
}

TABLE_PARAMETERS = {
    "javax.faces.partial.ajax": "true",
    "javax.faces.source": "formIzquierda:idArbolIndicadores",
    "javax.faces.partial.execute": "formIzquierda:idArbolIndicadores",
    "javax.faces.partial.render": "formIzquierda:panelIndicadores centro formIzquierda:idArbolIndicadores",
    "javax.faces.behavior.event": "select",
    "javax.faces.partial.event": "select",
    "formIzquierda:idArbolIndicadores_instantSelection": "",
    "formIzquierda": "formIzquierda",
    "formIzquierda:j_idt21": "0",
    "formIzquierda:j_idt29": "-1",
    "formIzquierda:j_idt50": "1",
    "formIzquierda:idAnioInicio": "1940",
    "formIzquierda:idAnioFin": "4279",
    "formIzquierda:idArbolIndicadores_selection": "",
    "javax.faces.ViewState": "",
}

DEFAULT_J_IDT = "centro:formSeccRes:resultado:0:j_idt89"

DETAIL_INDICATOR_PARAMETERS = {
    "javax.faces.partial.ajax": "true",
    "javax.faces.source": "",
    "javax.faces.partial.execute": "@all",
    "javax.faces.partial.render": "idPanelMetadato",
    "idIndicador": "",
    "centro:formSeccRes": "centro:formSeccRes",
    "javax.faces.ViewState": "",
}

EXCEL_PARAMETERS = {
    "javax.faces.ViewState": "",
}
