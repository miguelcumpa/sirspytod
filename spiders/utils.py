from scraper.constants import (
    DEFAULT_J_IDT,
    DETAIL_INDICATOR_PARAMETERS,
    EXCEL_PARAMETERS,
    INDICATOR_PARAMETERS,
    TABLE_PARAMETERS,
    TYPE_REQUEST,
)


def indicator_formdata(
    value_state: str,
    row_key: str = "",
    id_indicator: str = "",
    type_request=TYPE_REQUEST.INDICATOR,
    jidt=None,
):
    form_data = None
    if type_request == TYPE_REQUEST.INDICATOR:
        form_data = INDICATOR_PARAMETERS.copy()
        form_data["formIzquierda:idArbolIndicadores_expandNode"] = row_key
        form_data["javax.faces.ViewState"] = value_state
    elif type_request == TYPE_REQUEST.TABLE_PARAMETER:
        form_data = TABLE_PARAMETERS.copy()
        form_data["formIzquierda:idArbolIndicadores_instantSelection"] = row_key
        form_data["formIzquierda:idArbolIndicadores_selection"] = row_key
        form_data["javax.faces.ViewState"] = value_state
    elif type_request == TYPE_REQUEST.DETAIL_INDICATOR:
        form_data = DETAIL_INDICATOR_PARAMETERS.copy()
        form_data["javax.faces.ViewState"] = value_state
        form_data["idIndicador"] = id_indicator
        form_data["javax.faces.source"] = jidt or DEFAULT_J_IDT
        form_data[jidt or DEFAULT_J_IDT] = jidt or DEFAULT_J_IDT
    elif type_request == TYPE_REQUEST.EXCEL_PARAMETER:
        form_data = EXCEL_PARAMETERS.copy()
    return form_data


def remove_cdata(text: str) -> str:
    content = text.replace("<![CDATA[", "")
    content = content.replace("]]>", "")
    return content
