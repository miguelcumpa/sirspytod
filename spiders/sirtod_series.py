import logging
import re
from io import BytesIO
from time import sleep
from typing import Any

import scrapy
from datape.indicators.models import Data
from openpyxl import load_workbook
from scrapy.selector import Selector
from slugify import slugify

from scraper.constants import SCRAPING_DOMAINS, SIRTOD_BASE_URL, TYPE_REQUEST
from scraper.items import IndicatorItem

from .utils import indicator_formdata, remove_cdata

LOGGER = logging.getLogger(__name__)


class SirtodSeriesSpider(scrapy.Spider):
    name = "sirtod-series"
    base_url = SIRTOD_BASE_URL
    allowed_domains = SCRAPING_DOMAINS
    start_urls = [f"{base_url}/sirtod-series/"]
    app_url = f"{start_urls[0]}home.jsf"

    def parse(self, response, **kwargs):
        left_form = response.selector.css("#formIzquierda")
        value_state = left_form.css(
            'input[id="javax.faces.ViewState"]::attr(value)'
        ).extract_first()
        main_items = left_form.css(
            'div[id="formIzquierda:idArbolIndicadores"] ul[class="ui-tree-container"] li'
        )
        main_parents = []
        for item in main_items:
            main_parents.append(IndicatorItem.create_simple_indicator(item))

        for parent in main_parents:
            yield self.send_form_request(
                value_state,
                row_key=parent.row_key,
                meta={"parent": parent},
                type_request=TYPE_REQUEST.INDICATOR,
            )

    def fetch_item(self, response):
        parent = response.meta.get("parent")
        value_state = str(
            response.xpath(
                '//update[@id="javax.faces.ViewState"]/text()'
            ).extract_first()
        )
        items = Selector(
            text=response.xpath(
                '//update[@id="formIzquierda:idArbolIndicadores"]/text()'
            ).extract_first()
        ).xpath("//li")

        for item in items:
            try:
                chkbox_box = item.css("div.ui-chkbox.ui-widget")
                if len(chkbox_box) == 0:
                    current_parent = IndicatorItem.create_simple_indicator(
                        item, parent=parent
                    )
                    yield self.send_form_request(
                        value_state,
                        row_key=current_parent.row_key,
                        meta={"parent": current_parent},
                        type_request=TYPE_REQUEST.INDICATOR,
                    )
                elif len(chkbox_box) > 0:
                    name = item.css(
                        "span.ui-treenode-label.ui-corner-all span::text"
                    ).get()
                    row_key = item.css("::attr(data-rowkey)").get()
                    meta = {
                        "parent": parent,
                        "name": name,
                        "row_key": row_key,
                    }
                    yield self.send_form_request(
                        value_state,
                        row_key=row_key,
                        meta=meta,
                        type_request=TYPE_REQUEST.TABLE_PARAMETER,
                    )
            except Exception as e:
                LOGGER.error(str(e))

    def fetch_detail_item(self, response):
        def save_item(partial_response):
            value_state = str(  # noqa
                partial_response.xpath(
                    '//update[@id="javax.faces.ViewState"]/text()'
                ).extract_first()
            )
            form_data = partial_response.meta.get("excel_parameters")
            properties = Selector(text=remove_cdata(partial_response.text)).xpath(
                "//td"
            )
            default = dict(
                name=name.strip().title(), row_key=row_key, base_url=self.base_url
            )
            indicator = IndicatorItem.create_indicator(properties, parent, default)
            tmp_file = f"/tmp/{slugify(indicator.name)}.xlsx"
            form_data["javax.faces.ViewState"] = value_state
            return scrapy.FormRequest(
                self.app_url,
                formdata=form_data,
                callback=self.save_data,
                meta={"tmp_file": tmp_file, "indicator": indicator},
            )

        jidt = None
        name = response.meta.get("name")
        row_key = response.meta.get("row_key")
        parent = response.meta.get("parent")
        value_state = str(
            response.xpath(
                '//update[@id="javax.faces.ViewState"]/text()'
            ).extract_first()
        )
        onclicks = Selector(text=remove_cdata(response.text)).xpath(
            '//a[contains(@onclick, "PrimeFaces")]/@onclick'
        )

        if onclicks:
            compiled = re.compile(
                r".centro:formSeccRes:resultado:[0-9]{1,4}:j_idt[0-9]{1,4}"
            )
            result = compiled.search(onclicks[1].get())
            jidt = result.group()

        response_text = remove_cdata(response.text)
        id_indicator = re.search(
            "value:'(.+?)'",
            Selector(text=response_text)
            .xpath('//a[contains(@onclick, "PrimeFaces")]/@onclick')
            .get(),
        )
        table = Selector(text=response_text).css("table.ui-panelgrid")
        excel_parameters = dict()
        if table:
            btn_id = table.css("button::attr(id)").get()
            params = table.css('input[value="3"]::attr(id)').get().split(":")
            excel_parameters[":".join(params[:2])] = ":".join(params[:2])
            excel_parameters[":".join(params[:3])] = params[-1:]
            excel_parameters[btn_id] = ""

        if id_indicator:
            id_indicator = id_indicator.groups()[0]
            meta = {
                "name": name,
                "row_key": row_key,
                "parent": parent,
                "jidt": jidt,
                "excel_parameters": excel_parameters,
            }
            yield self.send_form_request(
                value_state,
                id_indicator=id_indicator,
                meta=meta,
                type_request=TYPE_REQUEST.DETAIL_INDICATOR,
                callback=save_item,
            )

    def save_data(self, partial_response: Any):
        tmp_file = partial_response.meta.get("tmp_file")
        indicator = partial_response.meta.get("indicator")
        workbook = load_workbook(filename=BytesIO(partial_response.body))
        workbook.save(tmp_file)
        workbook.close()
        Data.from_file(tmp_file, indicator)
        sleep(1)

    def send_form_request(
        self,
        value_state,
        row_key="",
        id_indicator="",
        meta=None,
        type_request=TYPE_REQUEST.INDICATOR,
        callback=None,
    ):
        if type_request == TYPE_REQUEST.INDICATOR:
            return scrapy.FormRequest(
                self.app_url,
                formdata=indicator_formdata(value_state, row_key=row_key),
                callback=self.fetch_item,
                meta=meta,
            )
        elif type_request == TYPE_REQUEST.TABLE_PARAMETER:
            return scrapy.FormRequest(
                self.app_url,
                formdata=indicator_formdata(
                    value_state,
                    row_key=row_key,
                    type_request=TYPE_REQUEST.TABLE_PARAMETER,
                ),
                callback=self.fetch_detail_item,
                meta=meta,
            )
        elif type_request == TYPE_REQUEST.DETAIL_INDICATOR:
            return scrapy.FormRequest(
                self.app_url,
                formdata=indicator_formdata(
                    value_state,
                    id_indicator=id_indicator,
                    type_request=TYPE_REQUEST.DETAIL_INDICATOR,
                ),
                callback=callback,
                meta=meta,
            )
